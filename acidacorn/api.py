from typing import Tuple

import acidacorn.wordlists


def roll(wordlist: str = "eff_large") -> Tuple[str, str]:
    wordlist_ = acidacorn.wordlists.wordlist(wordlist)
    return wordlist_.roll()


def generate_passphrase(
    wordlist: str = "eff_large", sep: str = " ", cap: str = "lower", num_words: int = 6
) -> str:
    wordlist_ = acidacorn.wordlists.wordlist(wordlist)
    return wordlist_.generate_passphrase(sep=sep, cap=cap, num_words=num_words)
