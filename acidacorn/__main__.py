import argparse
from typing import Optional, List

from acidacorn import api


def _parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        allow_abbrev=True,
        description="Generate secure and memorable passphrases from dice rolls.",
    )
    parser.add_argument(
        "--wordlist",
        default="eff_large",
        help="Dice wordlist.",
        choices=("eff_large", "eff_short", "eff_short_2", "diceware"),
    )
    parser.add_argument("--separator", default=" ", help="Word separator character.")
    parser.add_argument(
        "--capitalization",
        default="lower",
        help="Capitalization style.",
        choices=("lower", "upper", "title", "capitalize", "spongebob"),
    )
    parser.add_argument(
        "-n", "--num-words", type=int, default=6, help="Number of words."
    )
    return parser.parse_args(args)


def main():
    args = _parse_args()
    passphrase = api.generate_passphrase(
        wordlist=args.wordlist,
        sep=args.separator,
        cap=args.capitalization,
        num_words=args.num_words,
    )
    print(passphrase)


if __name__ == "__main__":
    main()
