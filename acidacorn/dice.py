import secrets


def roll(sides: int = 6, num: int = 5) -> str:
    return "".join(str(secrets.randbelow(sides) + 1) for _ in range(num))
