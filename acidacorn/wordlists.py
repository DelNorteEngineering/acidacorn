import collections.abc
import functools
import pathlib
import random
from typing import Dict, Iterator, Tuple, ValuesView

from acidacorn import dice


class Wordlist(collections.abc.Mapping):

    _dicewords: Dict[str, str]
    _sides: int
    _num_dice: int

    def __getitem__(self, key: str) -> str:
        return self._dicewords[key]

    def __iter__(self) -> Iterator[str]:
        return iter(self._dicewords)

    def __len__(self) -> int:
        return len(self._dicewords)

    @property
    def sides(self) -> int:
        return self._sides

    @property
    def num_dice(self) -> int:
        return self._num_dice

    def words(self) -> ValuesView:
        return self.values()

    def roll(self) -> Tuple[str, str]:
        key = dice.roll(sides=self.sides, num=self.num_dice)
        return key, self[key]

    def generate_passphrase(
        self, num_words: int = 6, sep: str = " ", cap: str = "lower"
    ) -> str:
        phrase = sep.join(self.roll()[1] for _ in range(num_words))
        if cap == 'lower':
            return phrase
        elif cap == 'upper':
            return phrase.upper()
        elif cap == 'title':
            return phrase.title()
        elif cap == 'capitalize':
            return phrase.capitalize()
        elif cap == 'spongebob':
            return ''.join(c.upper() if i % random.randint(2, 3) == 0 else c for i, c in enumerate(phrase))
        else:
            raise ValueError(f"Unsupported cap value: {cap}.")


class EFFLarge(Wordlist):
    def __init__(self):
        text = (
            pathlib.Path(__file__).parent / "wordlists" / "eff_large.txt"
        ).read_text()
        self._dicewords = {k: v for k, v in map(str.split, text.splitlines())}
        self._sides = 6
        self._num_dice = 5


class EFFShort(Wordlist):
    def __init__(self):
        text = (
            pathlib.Path(__file__).parent / "wordlists" / "eff_short.txt"
        ).read_text()
        self._dicewords = {k: v for k, v in map(str.split, text.splitlines())}
        self._sides = 6
        self._num_dice = 4


class EFFShort2(Wordlist):
    def __init__(self):
        text = (
            pathlib.Path(__file__).parent / "wordlists" / "eff_short_2.txt"
        ).read_text()
        self._dicewords = {k: v for k, v in map(str.split, text.splitlines())}
        self._sides = 6
        self._num_dice = 4


class Diceware(Wordlist):
    def __init__(self):
        text = (
            pathlib.Path(__file__).parent / "wordlists" / "diceware.txt"
        ).read_text()
        self._dicewords = {k: v for k, v in map(str.split, text.splitlines())}
        self._sides = 6
        self._num_dice = 5


_WORDLISTS = (
    "eff_large",
    "eff_short",
    "eff_short_2",
    "diceware",
)


@functools.lru_cache()
def wordlist(name: str = 'eff_large') -> Wordlist:
    if name == "eff_large":
        return EFFLarge()
    elif name == "eff_short":
        return EFFShort()
    elif name == "eff_short_2":
        return EFFShort2()
    elif name == "diceware":
        return Diceware()
    else:
        raise ValueError(
            f"Unrecognized wordlist: {name}. Wordlist choices: {_WORDLISTS}."
        )
