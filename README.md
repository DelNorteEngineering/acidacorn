[![pipeline status](https://gitlab.com/DelNorteEngineering/acidacorn/badges/master/pipeline.svg)](https://gitlab.com/DelNorteEngineering/acidacorn/-/commits/master)
[![coverage report](https://gitlab.com/DelNorteEngineering/acidacorn/badges/master/coverage.svg)](https://gitlab.com/DelNorteEngineering/acidacorn/-/commits/master)

# Install

```
$ python3 -m pip install pipx
$ pipx install git+https://gitlab.com/DelNorteEngineering/acidacorn.git#egg=acidacorn
$ acidacorn
```

# Usage

```
usage: acidacorn [-h] [--wordlist {eff_large,eff_short,eff_short_2,diceware}] [--separator SEPARATOR] [--capitalization {lower,upper,title,capitalize,spongebob}] [-n NUM_WORDS]

Generate secure and memorable passphrases from dice rolls.

optional arguments:
  -h, --help            show this help message and exit
  --wordlist {eff_large,eff_short,eff_short_2,diceware}
                        Dice wordlist.
  --separator SEPARATOR
                        Word separator character.
  --capitalization {lower,upper,title,capitalize,spongebob}
                        Capitalization style.
  -n NUM_WORDS, --num-words NUM_WORDS
                        Number of words.
```

# Example

```
$ acidacorn
survivor citric deepness footage matching plunging
```

```
$ acidacorn -n=4 --sep=_ --cap=spongebob
ProfilE_ItalIcS_ulTiMate_oVervAlUe
```
