import string

import acidacorn


def test_roll_eff_large():
    dice, word = acidacorn.roll(wordlist='eff_large')
    assert isinstance(dice, str)
    assert all(c in string.digits[1:7] for c in dice)
    assert isinstance(word, str)
    assert all(c in string.ascii_lowercase for c in word)
    assert word in acidacorn.wordlist('eff_large').words()


def test_roll_eff_short():
    dice, word = acidacorn.roll(wordlist='eff_short')
    assert isinstance(dice, str)
    assert all(c in string.digits[1:7] for c in dice)
    assert isinstance(word, str)
    assert all(c in string.ascii_lowercase for c in word)
    assert word in acidacorn.wordlist('eff_short').words()


def test_roll_eff_short_2():
    dice, word = acidacorn.roll(wordlist='eff_short_2')
    assert isinstance(dice, str)
    assert all(c in string.digits[1:7] for c in dice)
    assert isinstance(word, str)
    assert all(c in string.ascii_lowercase for c in word)
    assert word in acidacorn.wordlist('eff_short_2').words()


def test_roll_diceware():
    dice, word = acidacorn.roll(wordlist='diceware')
    assert isinstance(dice, str)
    assert all(c in string.digits[1:7] for c in dice)
    assert isinstance(word, str)
    assert all(c in string.ascii_lowercase + string.punctuation for c in word)  # diceware list has punctuation
    assert word in acidacorn.wordlist('diceware').words()


def test_generate_passphrase():
    wordlist = 'eff_large'
    sep = '_'
    cap = 'lower'
    passphrase = acidacorn.generate_passphrase(wordlist=wordlist, sep=sep, cap=cap)
    assert isinstance(passphrase, str)
    assert len(passphrase.split(sep)) == 6
    words = acidacorn.wordlist('eff_large').words()
    assert all(w in words for w in passphrase.split(sep))
