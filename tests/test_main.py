import argparse

from acidacorn import __main__ as main


def test_parse_args():
    args = main._parse_args([])
    assert args.separator == ' '
    assert args.capitalization == 'lower'
    assert args.num_words == 6

    args = main._parse_args(["--sep=_", "--cap=upper", "--num-words=4"])
    assert args.separator == '_'
    assert args.capitalization == 'upper'
    assert args.num_words == 4

    try:
        args = main._parse_args(["--cap=notsupported"])
    except (argparse.ArgumentError, SystemExit):
        pass
