from acidacorn import wordlists


def test_wordlist():
    wordlist = wordlists.wordlist('eff_large')
    assert isinstance(wordlist, wordlists.Wordlist)
    assert isinstance(wordlist, wordlists.EFFLarge)
    assert wordlist.num_dice == 5
    assert wordlist.sides == 6

    wordlist = wordlists.wordlist('eff_short')
    assert isinstance(wordlist, wordlists.Wordlist)
    assert isinstance(wordlist, wordlists.EFFShort)
    assert wordlist.num_dice == 4
    assert wordlist.sides == 6

    wordlist = wordlists.wordlist('eff_short_2')
    assert isinstance(wordlist, wordlists.Wordlist)
    assert isinstance(wordlist, wordlists.EFFShort2)
    assert wordlist.num_dice == 4
    assert wordlist.sides == 6

    wordlist = wordlists.wordlist('diceware')
    assert isinstance(wordlist, wordlists.Wordlist)
    assert isinstance(wordlist, wordlists.Diceware)
    assert wordlist.num_dice == 5
    assert wordlist.sides == 6

    try:
        _ = wordlists.wordlist('notawordlist')
    except ValueError:
        pass


def test_mapping():
    wordlist = wordlists.wordlist()
    assert '11111' in wordlist
    assert wordlist['11111'] == 'abacus'
    assert len(wordlist) == 7776


def test_generate_passphrase():
    wordlist = wordlists.wordlist()
    assert wordlist.generate_passphrase(cap='upper').isupper()
    assert wordlist.generate_passphrase(cap='title').istitle()
    assert not wordlist.generate_passphrase(cap='capitalize').islower()
    assert not wordlist.generate_passphrase(cap='spongebob').islower()
